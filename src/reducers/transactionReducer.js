import * as actionTypes from '../actions/types'

const initialState = {
    currentAccount:{amount:0,history:[]},
    savingsAccount:{amount:0,history:[]},
    creditCard:{amount:0,history:[]}
}

const reducer = (state=initialState,action)=>{
    switch(action.type){
        case actionTypes.TRANSACTION:
            console.log("In transaction reducer")
            // console.log(action);
            // console.log('oldstate: ', state)
            // const newState = {...state};
            const amount = action.payload.amount;
            const from = action.payload.from;
            const to = action.payload.to;
            return {
                ...state,
                [to]:{
                    amount:parseInt(state[to].amount) + parseInt(amount),
                    history:state[to].history.concat({
                        Date:Date.now(),
                        amount,
                        from
                    }) 
                },
                [from]:{
                    amount:parseInt(state[from].amount) - parseInt(amount),
                    history:state[from].history.concat({
                        Date:Date.now(),
                        amount,
                        to
                    }) 
                }
            }


    }

    return state;

}


export default reducer;