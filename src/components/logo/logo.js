import React from 'react'
import logoutLogo from '../../assets/logoutLogo.png'
import './logo.css'

const logo = (props) =>(
    <div className="Logo">
        <img src={logoutLogo} alt="logout Logo"/>
    </div>
)


export default logo;




