import React from 'react'
import './toolbar.css'
import Logo from '../../logo/logo'
import MenuLogo from '../../logo/menuLogo'
import callLogo from '../../../assets/call.png'
import callWhite from '../../../assets/callWhite.png'
import chatIcon from '../../../assets/chatIcon.png'
const toolbar = (props)=>(
    <>
    <header className="Toolbar">
        <MenuLogo/>
        <h1 style={{color:'white'}} className="fullTxt">BETTER BANKING</h1>
        <h1 style={{color:'white'}} className="shortTxt">BB</h1>
        <Logo/>
    </header>
    <header className="Toolbar2">
    <p className='fullTxt'>Feeling Lost? Let us help.</p>
    <button className='shortTxt' > <img id="callImg" src={callLogo}/> Call Us </button>
    <div className="leftAligned fullTxt">
    <img className="whiteLogo fullTxt"  src={callWhite}/>
    <p className="numberStyle fullTxt">482 186 425</p>
    </div>
    <button> <img id="callImg" src={chatIcon}/> Live Chat </button>
    </header>
</>
)

export default toolbar;
