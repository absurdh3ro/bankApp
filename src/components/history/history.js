import React from 'react'
import uniqueKey from '../../utils/uniqueKey'
import './history.css'

function history(props) {


     

    console.log("in history", props.hist);
    return (<div className={props.isArrowUp? 'showHistoryClass': 'historyStyle' }>
    <ul className='listClass'>
        {props.hist.map(elem=>{
            return <li key={uniqueKey()}>
                
                {elem.from ?
                 <div className="infoWrapperStyle">
                   <div className="fromStyle"> + <b>{elem.from} </b> <div className="dateStyle">{(new Date(elem.Date).toLocaleString())} </div> </div> <div className="fromAmountStyle">  <b> £{elem.amount} </b></div>
                   
                </div> 
               
                : null }
                
                {elem.to ?
                 <div className="infoWrapperStyle">
                   <div className="fromStyle"> - <b>{elem.to} </b> <div className="dateStyle">{(new Date(elem.Date).toLocaleString())} </div> </div> <div className="fromAmountStyle">  <b> £{elem.amount} </b></div>
                   
                </div> 
               
                : null }


{/* 
                {elem.to ?  <div>
                -{elem.to} => Amount: {elem.amount}
                </div> : null} */}
                
            </li>
        })}
    </ul>
    <button className='overViewBtnStyle'>Account Overview</button>
</div>)
}

export default history;