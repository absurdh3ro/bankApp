import React from 'react'
import Account from '../account/account'
import { connect } from 'react-redux';
import * as actionTypes from '../../actions/types'
import uniqueKey from '../../utils/uniqueKey'
import './accountContainer.css'

class accountContainer extends React.Component{

    constructor(props){
        super(props)
    }

    state = {
        transactionFrom:'currentAccount',
        transactionTo:'savingsAccount',
        amount:''
    }
    handleChange = (e)=>{
        this.setState({
            ...this.state,
            [e.target.name]:e.target.value
        });
        console.log(this.state);
    }

    render(){
        console.log("inContainer: ", this.props);
        const payload = {from:this.state.transactionFrom,to:this.state.transactionTo,amount:this.state.amount};
        return (<div >            
            <h1 className="titleTxt" >My Accounts</h1>
            <div className='container' style={{height:'100%'}}>
            <Account data={this.props.currentAccount} name='Current Account'  color="#218685"/>
            


            <Account data={this.props.savingsAccount} name='Savings Account' color="#395576"/>

    

            
            <Account data={this.props.creditCard} name='Credit Card' color="#693f57"/>
            

            </div>
            <h1 className="titleTxt">Quick Transfer</h1>

            <div className="inputWrapper">
                <div className="innerWrapper">
                <div className="amountInputStyle">
                    <p style={{display:"block"}}>I want to transfer</p>
                    <input placeholder="Enter Amount" style={{display:"block"}} type="number" value={this.state.amount} name='amount' onChange={this.handleChange}/>
                </div>
                <div className="selectFromStyle">
                    <p style={{display:"block"}}>Transaction from</p>
                    <select style={{display:"block"}} id="selectFrom" name='transactionFrom' value = {this.state.transactionFrom} onChange={this.handleChange}>
                        <option value = "currentAccount">Current Account</option>
                        <option value = "savingsAccount">Savings Account</option>
                        <option value = "creditCard">Credit Card</option>

                    </select>
                </div>
            
                <div className="selectToStyle">
                    <p style={{display:"block"}}>Transaction to</p>
                    <select style={{display:"block"}} id="selectTo" name='transactionTo' value = {this.state.transactionTo} onChange={this.handleChange}>
                        <option value = "currentAccount">Current Account</option>
                        <option value = "savingsAccount">Savings Account</option>
                        <option value = "creditCard">Credit Card</option>

                    </select>


                </div>
                <button className="transactionBtnStyle" onClick={()=>{
                    if(payload.from !== payload.to && payload.amount > 0){
                        this.props.doTransaction(payload)
                    }
                }
                }>Transfer Funds</button>
                
                </div>
            </div>
        </div>
        )
    }
  
}


const mapStateToProps = state =>{
    return {
        currentAccount:state.transaction.currentAccount,
        savingsAccount:state.transaction.savingsAccount,
        creditCard:state.transaction.creditCard,
        history:state.history
    }
}


const mapDispatchToProps = dispatch =>{
    return {
        doTransaction: (payload)=> dispatch({type:actionTypes.TRANSACTION, payload}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(accountContainer);