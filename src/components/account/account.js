import React, {useState} from 'react'
import './account.css'
import uniqueKey from '../../utils/uniqueKey'
import arrowUp from '../../assets/whiteArrowUp.png'
import arrowDown from '../../assets/whiteArrowDown.png'
import Histroy from '../history/history'

function Account(props){

    const [isArrowUp,setArrowUp] = useState(false);

    console.log('props in account: ',props);
    
    const clickHanlder = () =>{
        setArrowUp(!isArrowUp);
        console.log(isArrowUp);
    }


    return (
            <>
            
                <div className='account' style={{borderTopColor:props.color}}>
                    <div className="accountDetailsBig">
                        <div className='accountNameWrapper'> 
                            <p>{props.name}</p>
                            <div className='amountStyle'>
                                <p>£{props.data.amount}</p>

                            </div>
                            <img onClick={clickHanlder} className={'arrowImageStyle '} src={isArrowUp ? arrowUp : arrowDown}/>

                        </div>
                        <Histroy hist = {props.data.history} isArrowUp={isArrowUp}/>
                    </div>
            </div>
        </>
        )
}

export default Account;