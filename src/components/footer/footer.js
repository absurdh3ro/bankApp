import React from 'react'
import './footer.css'

function footer({ children }) {
    return (
      <div>
        <div className='HelperDivStyle' />
        <div className="FooterStyle">{children}</div>
      </div>
    );
  }

export default footer;