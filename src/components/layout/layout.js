import React from 'react'
import './layout.css'
import Toolbar from '../navigation/toolbar/toolbar'
import SideDrawer from '../navigation/sideDrawer/sideDrawer'
import Footer from '../footer/footer'

const layout =(props)=>(
    <>
    <div><Toolbar/> </div>
    <main className ='Content'>{props.children}</main>
    <Footer>Legal Info|Security|Accessibility</Footer>
    </>

)

export default layout