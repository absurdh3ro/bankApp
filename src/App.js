import React from 'react';
import './App.css';
import {Provider} from 'react-redux'
import store from './store'
import AccountContainer from './components/acoountContainer/accountContainer'
import Layout from './components/layout/layout'
function App() {
  return (
    <Provider store = {store}>
      
    <div>
      <Layout>
      <AccountContainer />
      </Layout>
    </div>
    </Provider>
  );
}

export default App;
